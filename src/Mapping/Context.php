<?php

namespace Vinds\AnnotationHydrator\Mapping;

class Context {

    /**
     * @var string[]
     */
    protected static $uniqueIdentifiers = [];

    /**
     * @var mixed
     */
    protected $input;

    /**
     * @var mixed
     */
    protected $output;

    /**
     * @var Context
     */
    protected $parentContext;

    /**
     * @var string
     */
    protected $uniqueIdentifier;


    /**
     * Context constructor.
     * @param $input
     * @param $output
     * @param null $parentContext
     */
    public function __construct($input, $output, $parentContext = null) {
        $this->parentContext = $parentContext;
        $this->input         = $input;
        $this->output        = $output;
    }

    /**
     * Текущий контекст
     * @return mixed
     */
    public function getInput() {
        return $this->input;
    }

    /**
     * @return mixed
     */
    public function getOutput() {
        return $this->output;
    }

    /**
     * Контекст текущего контекста
     * @return null|Context
     */
    public function parent() {
        return $this->parentContext;
    }

    /**
     * @return string
     */
    public function getUniqueIdentifier(): string {
        if ($this->uniqueIdentifier === null) {
            $this->uniqueIdentifier = $this->createUniqueIdentifier();
        }
        return $this->uniqueIdentifier;
    }

    /**
     * @return string
     */
    protected function createUniqueIdentifier(): string {
        $hash = spl_object_hash($this);
        while (isset(self::$uniqueIdentifiers[$hash])) {
            $hash = $hash . rand(1, 1000000);
        }

        self::$uniqueIdentifiers[$hash] = true;

        return $hash;
    }
}