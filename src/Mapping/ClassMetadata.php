<?php


namespace Vinds\AnnotationHydrator\Mapping;

class ClassMetadata {

    /**
     * @var EntityMap
     */
    protected $entityMap;

    /**
     * @var \ReflectionClass
     */
    public $refClass;

    /**
     * @var  \ReflectionProperty[]
     */
    protected $refProperties = [];

    /**
     * @var array
     */
    protected $fieldMappings = [];

    /**
     * @var array
     */
    protected $cache = [];


    /**
     * ClassMetadata constructor.
     * @param $name
     * @throws \ReflectionException
     */
    public function __construct($name) {
        $this->refClass = new \ReflectionClass($name);
    }

    /**
     * @param string $propertyName
     * @param FieldMap $map
     * @return $this
     */
    public function fieldMap(string $propertyName, FieldMap $map): ClassMetadata {
        $this->fieldMappings[$propertyName] = $map;
        return $this;
    }

    /**
     * @return EntityMap
     */
    public function getEntityMap(): ?EntityMap {
        return $this->entityMap;
    }

    /**
     * @param EntityMap|null $entityMap
     * @return ClassMetadata
     */
    public function setEntityMap(?EntityMap $entityMap): ClassMetadata {
        $this->entityMap = $entityMap;
        return $this;
    }

    /**
     * @param $name
     * @return \ReflectionProperty
     */
    public function getReflectionProperty($name): \ReflectionProperty {
        if (!isset($this->refProperties[$name])) {
            $this->refProperties[$name] = $this->refClass->getProperty($name);
            $this->refProperties[$name]->setAccessible(true);
        }
        return $this->refProperties[$name];
    }

    /**
     * @return FieldMap[]
     */
    public function getFieldMappings(): array {
        return $this->fieldMappings;
    }

    /**
     * @param $hydrateName
     * @return null|FieldMap
     */
    public function findFindMapByHydrateName($hydrateName): ?FieldMap {
        if (isset($this->cache['byHydrateName'][$hydrateName])) {
            return $this->cache['byHydrateName'][$hydrateName];
        }

        foreach ($this->getFieldMappings() as $fieldMap) {
            if ($fieldMap->hydrateName === $hydrateName) {
                $this->cache['byHydrateName'][$hydrateName] = $fieldMap;
                return $fieldMap;
                break;
            }
        }

        return null;
    }

}