<?php

namespace Vinds\AnnotationHydrator\Mapping;

use Vinds\AnnotationHydrator\Strategy\StrategyInterface;

class FieldMap implements \Serializable {

    /**
     * @var EntityMap
     */
    public $entityMap;

    /**
     * @var bool
     */
    public $primary = false;

    /**
     * @var string
     */
    public $name;

    /**
     * Название поля массива из которого берется значение при создание сущьности
     * @var string
     */
    public $hydrateName;

    /**
     * Название поля массива куда извлекается значение свойства
     * @var
     */
    public $extractName;

    /**
     * @var string
     */
    public $type;

    /**
     * @var StrategyInterface
     */
    public $strategy;

    /**
     * @var bool
     */
    public $readOnly = false;

    /**
     * @var bool
     */
    public $propertyPublic = false;

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var \ReflectionProperty
     */
    public $reflectionProperty;

    /**
     * @var array
     */
    public $options = [];

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize() {
        return serialize([
            'entityMap'          => $this->entityMap,
            'primary'            => $this->primary,
            'name'               => $this->name,
            'hydrateName'        => $this->hydrateName,
            'extractName'        => $this->extractName,
            'type'               => $this->type,
            'strategy'           => get_class($this->strategy),
            'readOnly'           => $this->readOnly,
            'propertyPublic'     => $this->propertyPublic,
            'multiple'           => $this->multiple,
            'reflectionProperty' => [
                'class' => $this->reflectionProperty->class,
                'name'  => $this->reflectionProperty->name,
            ],
            'options'            => $this->options,
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     * @throws \ReflectionException
     */
    public function unserialize($serialized) {
        $data = unserialize($serialized);

        $this->entityMap          = $data['entityMap'];
        $this->primary            = $data['primary'];
        $this->name               = $data['name'];
        $this->hydrateName        = $data['hydrateName'];
        $this->extractName        = $data['extractName'];
        $this->type               = $data['type'];
        $this->strategy           = $data['strategy'];
        $this->readOnly           = $data['readOnly'];
        $this->propertyPublic     = $data['propertyPublic'];
        $this->multiple           = $data['multiple'];
        $this->reflectionProperty = new \ReflectionProperty(
            $data['reflectionProperty']['class'],
            $data['reflectionProperty']['name']
        );
        $this->options            = $data['options'];
    }
}