<?php

namespace Vinds\AnnotationHydrator\Mapping;

class EntityMap implements \Serializable {

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var \ReflectionClass
     */
    public $reflectionClass;

    /**
     * @inheritDoc
     */
    public function serialize() {
        return serialize([
            'options' => $this->options,
            'reflectionClass' => $this->reflectionClass->name,
        ]);
    }

    /**
     * @inheritDoc
     * @throws \ReflectionException
     */
    public function unserialize($serialized) {
        $data = unserialize($serialized);

        $this->options            = $data['options'];
        $this->reflectionClass    = new \ReflectionClass($data['reflectionClass']);
    }
}