<?php


namespace Vinds\AnnotationHydrator\Reference;


class LazyValue {

    /**
     * @var mixed
     */
    protected $key;

    /**
     * @var LazyStorage
     */
    protected $storage;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var callable|null
     */
    protected $injection;

    /**
     * LazyValue constructor.
     * @param mixed $key
     * @param LazyStorage $storage
     */
    public function __construct($key, LazyStorage $storage) {
        $this->key = $key;
        $this->storage = $storage;
    }

    /**
     * @return mixed
     */
    public function get() {
        if ($this->value === null) {
            $this->value = $this->storage->get($this->key);

            if ($this->injection !== null) {
                call_user_func_array($this->injection, [$this->value]);
            }
        }

        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @param callable|null $injection
     * @return $this
     */
    public function setInjection(?callable $injection) {
        $this->injection = $injection;
        return $this;
    }
}