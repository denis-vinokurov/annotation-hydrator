<?php

namespace Vinds\AnnotationHydrator\Reference;

use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydrator\Exception\LogicException;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Repository\RepositoryInterface;

class LazyStorage {

    /**
     * @var array
     */
    protected $storage;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var string
     */
    protected $field;

    /**
     * @var LazyValue[]
     */
    protected $values = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var bool
     */
    protected $oneToMany = false;


    /**
     * LazyStorage constructor.
     * @param RepositoryInterface $repository
     * @param string $field
     * @param EntityManager $entityManager
     */
    public function __construct(RepositoryInterface $repository, string $field, EntityManager $entityManager) {
        $this->repository = $repository;
        $this->field = $field;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $key
     * @return LazyValue
     */
    public function createLazyValue($key): LazyValue {
        foreach ($this->values as $item) {
            if ($item['key'] === $key) {
                return $item['value'];
            }
        }

        $lazyValue = new LazyValue($key, $this);

        $this->values[] = [
            'key'   => $key,
            'value' => $lazyValue,
        ];

        return $lazyValue;

    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function get($key) {
        if ($this->storage === null) {
            $this->load();
        }

        return $this->storage[$key];
    }

    /**
     * @return void
     */
    protected function load() {
        $this->storage = [];

        $keys = [];
        foreach ($this->values as $item) {
            $keys[] = $item['key'];
        }

        if (empty($keys)) {
            return;
        }

        $result = $this->repository->getList()
            ->whereIn($this->field, $keys)
            ->fetch();

        /** @var FieldMap $refFieldMap */
        $refFieldMaps = null;
        foreach ($result as $item) {
            if (is_object($item)) {
                $className = get_class($item);

                if ($refFieldMap === null || $refFieldMap->reflectionProperty->class !== $className) {
                    $refFieldMap = $this->entityManager->getClassMetadata($className)->findFindMapByHydrateName($this->field);

                    if ($refFieldMap === null) {
                        throw new LogicException('Не удалось получить метаданные для поля "' . $this->field . '"');
                    }
                }

                $key = $refFieldMap->reflectionProperty->getValue($item);
            } else {
                $key = $item[$this->field];
            }

            if ($this->oneToMany) {
                if ($key instanceof LazyValue) {
                    $key = $key->getKey();
                }

                if (empty($this->storage[$key])) {
                    $this->storage[$key] = [];
                }
                $this->storage[$key][] = $item;
            } else {
                $this->storage[$key] = $item;
            }
        }
    }

    /**
     * @param bool $oneToMany
     * @return $this
     */
    public function setOneToMany(bool $oneToMany) {
        $this->oneToMany = $oneToMany;
        return $this;
    }
}
