<?php

namespace Vinds\AnnotationHydrator\Repository;

interface ListInterface {

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function whereIn(string $field, array $value);

    /**
     * @return iterable
     */
    public function fetch();
}