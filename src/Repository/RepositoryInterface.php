<?php


namespace Vinds\AnnotationHydrator\Repository;


interface RepositoryInterface {

    /**
     * @return ListInterface
     */
    public function getList(): ListInterface;
}