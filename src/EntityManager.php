<?php

namespace Vinds\AnnotationHydrator;

use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Cache\Cache;
use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\Annotations\Field;
use Vinds\AnnotationHydrator\Annotations\Multiple;
use Vinds\AnnotationHydrator\Annotations\Options;
use Vinds\AnnotationHydrator\Annotations\Postfix;
use Vinds\AnnotationHydrator\Annotations\Prefix;
use Vinds\AnnotationHydrator\Annotations\Primary;
use Vinds\AnnotationHydrator\Exception\InvalidArgumentException;
use Vinds\AnnotationHydrator\Exception\UnexpectedValueException;
use Vinds\AnnotationHydrator\Mapping\ClassMetadata;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\EntityMap;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyInterface;
use Vinds\AnnotationHydrator\Strategy\StrategyWithContainerInterface;

class EntityManager {

    /**
     * @var CachedReader
     */
    protected $annotationReader;

    /**
     * @var array
     */
    protected $loadedMetadata = [];

    /**
     * @var StrategyInterface[]
     */
    protected $strategies = [];

    /**
     * @var Cache
     */
    protected $cacheDriver;
    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * EntityManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->annotationReader = $container->get(Reader::class);
        $this->cacheDriver = $container->get(Cache::class);
    }

    /**
     * @param $name
     * @return mixed|ClassMetadata
     * @throws \ReflectionException
     */
    public function getClassMetadata($name) {
        if (isset($this->loadedMetadata[$name])) {
            return $this->loadedMetadata[$name];
        }

        $class = new ClassMetadata($name);
        $this->loadedMetadata[$name] = $class;

        $this->fillEntityMap($class);
        $this->fillFieldMapping($class);

        return $class;
    }

    /**
     * @param string $name
     * @return StrategyInterface
     */
    public function getStrategy(string $name) {
        if (!isset($this->strategies[$name])) {
            if ($this->container->has($name)) {
                $strategy = $this->container->get($name);
            } elseif (in_array(StrategyWithContainerInterface::class, class_implements($name))) {
                $strategy = new $name($this->container);
            } else {
                $strategy = new $name;
            }

            $this->strategies[$name] = $strategy;
        }
        return $this->strategies[$name];
    }

    /**
     * @param string $name
     * @param array $data
     * @param Context|null $context
     * @return object
     */
    public function hydrate(string $name, array $data, Context $context = null) {
        try {
            $class = $this->getClassMetadata($name);
        } catch (\ReflectionException $e) {
            throw new InvalidArgumentException("Не удается получить метаданные для класса '${name}'");
        }

        $entity = $class->refClass->newInstanceWithoutConstructor();
        // Создаем контекст для текущего объекта
        $context = new Context($data, $entity, $context);
        foreach ($class->getFieldMappings() as $propertyName => $fieldMapping) {
            if (!array_key_exists($fieldMapping->hydrateName, $data)) {
                continue;
            }

            $property = $fieldMapping->reflectionProperty;
            $value = $this->execPreSetValue($fieldMapping, $data[$fieldMapping->hydrateName], $context);


            if ($fieldMapping->propertyPublic) {
                $entity->{$property->name} = $value;
            } else {
                $property->setValue($entity, $value);
            }
        }

        return $entity;
    }

    /**
     * @param $object
     * @return array
     */
    public function extract($object) {
        $className = get_class($object);

        try {
            $class = $this->getClassMetadata($className);
        } catch (\ReflectionException $e) {
            throw new InvalidArgumentException("Не удается получить метаданные для класса '${className}'");
        }

        $data = new \ArrayObject();

        $context = new Context($object, $data);

        foreach ($class->getFieldMappings() as $propertyName => $fieldMapping) {
            if ($fieldMapping->primary || $fieldMapping->readOnly) {
                continue;
            }

            $property = $class->refClass->getProperty($propertyName);
            $property->setAccessible(true);

            $value = $this->execExtractValue($fieldMapping, $property->getValue($object), $context);

            $data[$fieldMapping->extractName] = $value;
        }

        return $data->getArrayCopy();
    }

    /**
     * @param string $name
     * @param array $data
     * @return array
     */
    public function hydrateCollection(string $name, array $data) {
        $collection = new \ArrayObject();
        $context = new Context($data, $collection);

        foreach ($data as $item) {
            $collection[] = $this->hydrate($name, $item, $context);
        }
        return $collection->getArrayCopy();
    }

    /**
     * @param $object
     * @param $primary
     */
    public function setPrimary($object, $primary) {
        if ($primary === null) {
            throw new InvalidArgumentException("Значение 'null' не допустимо для аргумента \$primary");
        }

        $className = get_class($object);

        try {
            $metadata = $this->getClassMetadata($className);
        } catch (\ReflectionException $e) {
            throw new InvalidArgumentException("Не удается получить метаданные для класса '${className}'");
        }

        $primaryMapField = null;

        foreach ($metadata->getFieldMappings() as $fieldMap) {
            if ($fieldMap->primary) {
                $primaryMapField = $fieldMap;
                break;
            }
        }

        if ($primaryMapField === null) {
            throw new UnexpectedValueException("У класса '${className}' нет первичного ключа");
        }

        if (!$primaryMapField->propertyPublic) {
            $primaryMapField->reflectionProperty->setAccessible(true);
        }

        $primaryMapField->reflectionProperty->setValue($object, $this->execPreSetValue($primaryMapField, $primary));
    }

    /**
     * @param \ReflectionClass $class
     * @return null|Mapping\EntityMap
     */
    protected function createEntityMap(\ReflectionClass $class): ?Mapping\EntityMap {
        $annotations = $this->annotationReader->getClassAnnotations($class);

        if (empty($annotations)) {
            return null;
        }

        $map = new EntityMap();
        $map->reflectionClass = $class;

        foreach ($annotations as $annotation) {
            if ($annotation instanceof Options) {
                $map->options = array_merge($map->options, $annotation->value);
            }
        }

        return $map;
    }

    /**
     * @param \ReflectionProperty $property
     * @return FieldMap|null
     */
    protected function createFieldMap(\ReflectionProperty $property): ?FieldMap {
        $annotations = $this->annotationReader->getPropertyAnnotations($property);

        // Если нет аннотаций то не создаем FieldMap
        if (empty($annotations)) {
            return null;
        }

        $map = new FieldMap();
        $map->reflectionProperty = $property;
        $map->propertyPublic = $property->isPublic();
        if (!$map->propertyPublic) {
            $property->setAccessible(true);
        }

        foreach ($annotations as $annotation) {
            if ($annotation instanceof Primary) {
                $map->primary = true;
            }

            if ($annotation instanceof Field) {
                $map->type = get_class($annotation);
                $map->name = $annotation->name;
                $map->strategy = $annotation->strategy;
                $map->hydrateName = $annotation->name;
                $map->extractName = $annotation->name;
                $map->readOnly = $annotation->readOnly;

                if (!empty($annotation->options)) {
                    $map->options = array_merge($map->options, $annotation->options);
                }
            }

            // Множественное поля
            if ($annotation instanceof Multiple) {
                $map->multiple = true;
            }

            if ($annotation instanceof Options) {
                $map->options = array_merge($map->options, $annotation->value);
            }
        }

        foreach ($map->options as $option) {
            if ($option instanceof Prefix) {
                $prefix = $option;
            }

            if ($option instanceof Postfix) {
                $postfix = $option;
            }
        }

        if (!empty($prefix)) {
            $map->hydrateName = ($prefix->hydrate !== null ? $prefix->hydrate : $prefix->create) . $map->name;
            $map->extractName = $prefix->extract . $map->name;
        }

        if (!empty($postfix)) {
            $map->hydrateName = $map->hydrateName . ($postfix->hydrate !== null ? $postfix->hydrate : $postfix->create);
            $map->extractName = $map->extractName . $postfix->extract;
        }

        return $map;
    }

    /**
     * @param FieldMap $fieldMapping
     * @param $value
     * @param null|Context $context
     * @return mixed
     */
    protected function execPreSetValue(FieldMap $fieldMapping, $value, ?Context $context = null) {
        $callback = null;
        if ($fieldMapping->strategy !== null) {
            $callback = [$fieldMapping->strategy, 'hydrate'];
        }


        if (is_callable($callback)) {
            if ($fieldMapping->multiple) {
                if (!empty($value)) {
                    foreach ($value as &$subValue) {
                        $subValue = call_user_func_array($callback, [$subValue, $fieldMapping, $context]);
                    }
                }
            } else {
                $value = call_user_func_array($callback, [$value, $fieldMapping, $context]);
            }
        }

        return $value;
    }

    /**
     * @param FieldMap $fieldMapping
     * @param $value
     * @param null|Context $context
     * @return mixed
     */
    protected function execExtractValue(FieldMap $fieldMapping, $value, ?Context $context = null) {
        $callback = null;
        if ($fieldMapping->strategy !== null) {
            $callback = [$fieldMapping->strategy, 'extract'];
        }

        if ($callback !== null) {
            if ($fieldMapping->multiple) {
                if (!empty($value)) {
                    foreach ($value as &$subValue) {
                        $subValue = call_user_func_array($callback, [$subValue, $fieldMapping, $context]);
                    }
                }
            } else {
                $value = call_user_func_array($callback, [$value, $fieldMapping, $context]) ;
            }
        }

        return $value;
    }

    /**
     * @param ClassMetadata $class
     */
    protected function fillEntityMap(ClassMetadata $class) {
        $cacheId = sha1(__METHOD__ . $class->refClass->name . filemtime($class->refClass->getFileName()));

        if (false !== ($cached = $this->cacheDriver->fetch($cacheId))) {
            /** @var EntityMap $map */
            $map = $cached;

            if ($map instanceof EntityMap) {
                $map->reflectionClass = $class->refClass;
            }
        } else {
            $map = $this->createEntityMap($class->refClass);
            $this->cacheDriver->save($cacheId, $map);
        }

        if ($map !== null) {
            $class->setEntityMap($map);
        }
    }

    /**
     * @param ClassMetadata $class
     */
    protected function fillFieldMapping(ClassMetadata $class) {
        // К ключу добавляем дату изменеие файла в котором объявляется класс
        $cacheId = sha1(__METHOD__ . $class->refClass->name . filemtime($class->refClass->getFileName()));

        $entityMap = $class->getEntityMap();

        if (false !== ($cached = $this->cacheDriver->fetch($cacheId))) {
            // Если кеши существуют заполняем данные
            /** @var FieldMap[] $cached */
            foreach ($cached as $key => $value) {

                $value->entityMap = $entityMap;
                if ($value->strategy !== null && is_string($value->strategy)) {
                    $value->strategy = $this->getStrategy($value->strategy);
                }

                $value->reflectionProperty = $class->getReflectionProperty($value->reflectionProperty->name);
                $class->fieldMap($key, $value);
            }
        } else {
            // Если нет закешированных данных создаем и добавляем в кеш
            foreach ($class->refClass->getProperties() as $property) {
                $fieldMap = $this->createFieldMap($property);

                // Если не создалась карто то пропускаем
                if ($fieldMap === null) {
                    continue;
                }

                $fieldMap->entityMap = $entityMap;
                if ($fieldMap->strategy !== null) {
                    $fieldMap->strategy = $this->getStrategy($fieldMap->strategy);
                }

                $class->fieldMap($property->name, $fieldMap);

            }

            $this->cacheDriver->save($cacheId, $class->getFieldMappings());
        }
    }
}