<?php

namespace Vinds\AnnotationHydrator\Annotations;

use Vinds\AnnotationHydrator\Strategy\FloatStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class FloatField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class FloatField extends Field {

    /**
     * @var string
     */
    public $strategy = FloatStrategy::class;
}