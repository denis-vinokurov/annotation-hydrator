<?php


namespace Vinds\AnnotationHydrator\Annotations;
use Doctrine\Common\Annotations\Annotation\Target;


/**
 * @Annotation()
 * @Target({"PROPERTY", "CLASS"})
 * Class Options
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Options {

    /**
     * @var array
     */
    public $value;
}