<?php

namespace Vinds\AnnotationHydrator\Annotations;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation()
 * @Target({"ANNOTATION"})
 * Class Postfix
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Postfix {

    /**
     * @var string
     */
    public $hydrate = '';

    /**
     * @var string
     */
    public $extract = '';

}