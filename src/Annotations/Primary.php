<?php

namespace Vinds\AnnotationHydrator\Annotations;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation()
 * @Target({"PROPERTY"})
 * Class Primary
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Primary {

}