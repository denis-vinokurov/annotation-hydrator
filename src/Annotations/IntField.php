<?php

namespace Vinds\AnnotationHydrator\Annotations;

use Vinds\AnnotationHydrator\Strategy\IntStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class Field
 * @package Vinds\AnnotationHydrator\Annotations
 */
class IntField extends Field {
    /**
     * @var string
     */
    public $strategy = IntStrategy::class;
}