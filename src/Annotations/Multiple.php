<?php

namespace Vinds\AnnotationHydrator\Annotations;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class Multiple
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Multiple {

}