<?php


namespace Vinds\AnnotationHydrator\Annotations;

use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Strategy\DateTimeStrategy;


/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class DateTimeField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class DateTimeField extends Field {

    public $strategy = DateTimeStrategy::class;
}