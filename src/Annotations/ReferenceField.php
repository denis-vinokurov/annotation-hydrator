<?php


namespace Vinds\AnnotationHydrator\Annotations;

use Vinds\AnnotationHydrator\Strategy\ReferenceStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class ReferenceField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class ReferenceField extends Field {

    /**
     * @var string
     */
    public $strategy = ReferenceStrategy::class;

    /**
     * @var string
     */
    public $repository;

    /**
     * @var string
     */
    public  $referenceField;

    /**
     * @var bool
     */
    public $oneToMany = false;

    /**
     * @var bool
     */
    public $manyToOne = false;

    public function __construct($value) {
        $this->name = $value['name'];
        $this->readOnly = isset($value['readOnly']) && $value['readOnly'] === true;

        $this->options['repository'] = $value['repository'];
        $this->options['referenceField'] = $value['referenceField'];
        $this->options['oneToMany'] = isset($value['oneToMany']) ? $value['oneToMany'] : $this->oneToMany;
        $this->options['manyToOne'] = isset($value['manyToOne']) ? $value['manyToOne']: $this->manyToOne;
    }
}