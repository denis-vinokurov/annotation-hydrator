<?php


namespace Vinds\AnnotationHydrator\Annotations;
use Doctrine\Common\Annotations\Annotation\Target;


/**
 * @Annotation()
 * @Target({"ANNOTATION"})
 * Class Prefix
 * @package Vinds\AnnotationHydrator\Annotations
 */
final class Prefix {

    /**
     * @var string
     */
    public $hydrate = '';

    /**
     * @var string
     */
    public $extract = '';
}