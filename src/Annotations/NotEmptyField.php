<?php


namespace Vinds\AnnotationHydrator\Annotations;

use Vinds\AnnotationHydrator\Strategy\NotEmptyStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class NotEmptyField
 * @package Vinds\AnnotationHydrator\Annotations
 */
class NotEmptyField extends Field {

    /**
     * @var string
     */
    public $strategy = NotEmptyStrategy::class;
}