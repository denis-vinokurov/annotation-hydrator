<?php

namespace Vinds\AnnotationHydrator\Annotations;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"CLASS"})
 * Class Entity
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Entity {

}