<?php

namespace Vinds\AnnotationHydrator\Annotations;

use Vinds\AnnotationHydrator\Strategy\StringStrategy;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class Field
 * @package Vinds\AnnotationHydrator\Annotations
 */
class StringField extends Field {

    /**
     * @var string
     */
    public $strategy = StringStrategy::class;
}