<?php


namespace Vinds\AnnotationHydrator\Annotations;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class Field
 * @package Vinds\AnnotationHydrator\Annotations
 */
class Field {
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $strategy;

    /**
     * @var bool
     */
    public $readOnly = false;

    /**
     * @var array
     */
    public $options = [];
}