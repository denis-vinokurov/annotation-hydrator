<?php


namespace Vinds\AnnotationHydrator\Strategy;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

interface StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context);

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context);
}