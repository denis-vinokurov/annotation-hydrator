<?php


namespace Vinds\AnnotationHydrator\Strategy;

use Psr\Container\ContainerInterface;

interface StrategyWithContainerInterface extends StrategyInterface {

    /**
     * StrategyWithContainerInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);
}