<?php

namespace Vinds\AnnotationHydrator\Strategy;

use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydrator\Exception\LogicException;
use Vinds\AnnotationHydrator\Exception\UnexpectedValueException;
use Vinds\AnnotationHydrator\Reference\LazyStorage;
use Vinds\AnnotationHydrator\Reference\LazyValue;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class ReferenceStrategy implements StrategyWithContainerInterface {

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var LazyStorage[]
     */
    protected $storageList = [];

    /**
     * StrategyWithContainerInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return null|LazyValue
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?LazyValue {
        if (empty($value)) {
            return null;
        }

        $result = $this->getStorage($fieldMap, $context)->createLazyValue($value);

        $result->setInjection($this->createInjection($fieldMap, $context));

        return $result;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof LazyValue) {
            return $value->getKey();
        }

        if ($fieldMap->options['oneToMany']) {
            $item = reset($value);
        } else {
            $item = $value;
        }

        if (empty($item)) {
            return null;
        }

        if (is_array($item)) {
            return $item[$fieldMap->options['referenceField']];
        }

        if (is_object($item)) {
            /** @var EntityManager $entityManager */
            $entityManager = $this->container->get(EntityManager::class);
            $metadata      = $entityManager->getClassMetadata(get_class($item));

            foreach ($metadata->getFieldMappings() as $fieldMapping) {
                if ($fieldMap->options['referenceField'] === $fieldMapping->hydrateName) {
                    return $fieldMapping->reflectionProperty->getValue($item);
                }
            }
        }

        $field = $fieldMap->hydrateName;
        $className = $fieldMap->reflectionProperty->class;
        throw new UnexpectedValueException(
            "Не удолось извлечь значение для поля типа 'Ссылка'. 
            Сущьность: ${className}. Поля: ${field}. Значение: ${item}."
        );
    }

    /**
     * @param Context $context
     * @return string
     */
    protected function getContextKey(Context $context) {
        if ($context->parent() !== null) {
            return $context->parent()->getUniqueIdentifier();
        } else {
            return $context->getUniqueIdentifier();
        }
    }

    /**
     * @param FieldMap $fieldMap
     * @param Context $context
     * @return LazyStorage
     */
    protected function getStorage(FieldMap $fieldMap, Context $context): LazyStorage {
        $key = $this->getContextKey($context) . spl_object_hash($fieldMap);

        if (!isset($this->storageList[$key])) {
            if (empty($fieldMap->options['repository'])) {
                throw new LogicException(sprintf(
                    'Не указан репозиторий для поля: %s::%s',
                    $fieldMap->reflectionProperty->class,
                    $fieldMap->name
                ));
            }
            if (empty($fieldMap->options['referenceField'])) {
                throw new LogicException(sprintf(
                    'Не указано поля привязки для поля: %s::%s',
                    $fieldMap->reflectionProperty->class,
                    $fieldMap->name
                ));
            }

            $storage = new LazyStorage(
                $this->container->get($fieldMap->options['repository']),
                $fieldMap->options['referenceField'],
                $this->container->get(EntityManager::class)
            );

            $storage->setOneToMany((bool) $fieldMap->options['oneToMany']);

            $this->storageList[$key] = $storage;
        }

        return $this->storageList[$key];
    }

    /**
     * @param FieldMap $fieldMap
     * @param Context $context
     * @return callable|null
     */
    protected function createInjection(FieldMap $fieldMap, Context $context): ?callable {
        if (!$fieldMap->options['oneToMany']) {
            return null;
        }

        // Если поля является привязкой один ко многим и связующие поля является привязкой типа многие к одному,
        // то записываем в значения связующиего поля результирующий объект
        return function ($items) use ($fieldMap, $context) {
            if (empty($items)) {
                return;
            }

            $item = reset($items);

            if (is_array($item)) {
                return;
            }

            /** @var EntityManager $entityManager */
            $entityManager = $this->container->get(EntityManager::class);

            $field = $entityManager->getClassMetadata(get_class($item))
                ->findFindMapByHydrateName($fieldMap->options['referenceField']);

            if ($field->options['manyToOne']) {
                foreach ($items as $item) {
                    $field->reflectionProperty->setValue($item, $context->getOutput());
                }
            }
        };
    }
}