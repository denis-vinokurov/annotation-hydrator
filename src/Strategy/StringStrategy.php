<?php


namespace Vinds\AnnotationHydrator\Strategy;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class StringStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return null|string
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?string {
        if ($value === null) {
            return null;
        }

        return (string) $value;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param null|string $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }
}