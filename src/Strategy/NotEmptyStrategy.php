<?php


namespace Vinds\AnnotationHydrator\Strategy;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class NotEmptyStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param mixed $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return bool
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): bool {
        return !empty($value);
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param bool $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return bool|null
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context): ?bool {
        return $value;
    }
}