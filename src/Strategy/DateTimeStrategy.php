<?php

namespace Vinds\AnnotationHydrator\Strategy;

use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class DateTimeStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?\DateTime {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof \DateTime) {
            return $value;
        }

        return new \DateTime($value);
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param null|\DateTime $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }
}