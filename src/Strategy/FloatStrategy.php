<?php


namespace Vinds\AnnotationHydrator\Strategy;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class FloatStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return float|null
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?float {
        if ($value === null) {
            return null;
        }

        return (float)$value;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param null|float $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }
}