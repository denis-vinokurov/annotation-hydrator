<?php


namespace Vinds\AnnotationHydrator\Strategy;


use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;

class IntStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return int|null
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context): ?int {
        if ($value === null) {
            return null;
        }

        return (int)$value;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param null|int $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }
}