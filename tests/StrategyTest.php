<?php


namespace Vinds\AnnotationHydrator\Tests;


use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Zend\ServiceManager\ServiceManager;

class StrategyTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     */
    protected function setUp() {
        parent::setUp();

        $this->container = new ServiceManager(require __DIR__ . '/config.php');
        $this->entityManager = new EntityManager($this->container);
    }

    public function testStrategyWithContainer() {
        /** @var StrategyTest\StrategyWithContainer $strategy */
        $strategy = $this->entityManager->getStrategy(StrategyTest\StrategyWithContainer::class);

        $this->assertEquals($strategy->getContainer(), $this->container);
    }
}