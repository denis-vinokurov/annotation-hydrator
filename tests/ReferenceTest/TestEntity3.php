<?php


namespace Vinds\AnnotationHydrator\Tests\ReferenceTest;
use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\IntField;

/**
 * @Entity()
 * Class TestEntity3
 * @package Vinds\AnnotationHydrator\Tests\ReferenceTest
 */
class TestEntity3 {

    /**
     * @IntField(name="id")
     * @var int
     */
    public $id;
}