<?php

namespace Vinds\AnnotationHydrator\Tests\ReferenceTest;

use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydrator\Annotations\ReferenceField;
use Vinds\AnnotationHydrator\Reference\LazyValue;

/**
 * @Entity()
 * Class TestEntity2
 * @package Vinds\AnnotationHydrator\Tests\ReferenceTest
 */
class TestEntity2 {

    /**
     * @IntField(name="id")
     * @var int
     */
    public $id;

    /**
     * @ReferenceField(name="manyToOne", repository="repository1", referenceField="id", manyToOne=true)
     * @var TestEntity1
     */
    public $manyToOne;

    /**
     * @return TestEntity1
     */
    public function getManyToOne(): TestEntity1 {
        if ($this->manyToOne instanceof LazyValue) {
            $this->manyToOne = $this->manyToOne->get();
        }
        return $this->manyToOne;
    }
}