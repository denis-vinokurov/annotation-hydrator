<?php


namespace Vinds\AnnotationHydrator\Tests\ReferenceTest;


use Vinds\AnnotationHydrator\EntityManager;

class TestCollectionList implements \Vinds\AnnotationHydrator\Repository\ListInterface {

    private $data;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var string
     */
    private $entityClass;

    public function __construct($data, EntityManager $entityManager, string $entityClass) {
        $this->data          = $data;
        $this->entityManager = $entityManager;
        $this->entityClass   = $entityClass;
    }

    /**
     * @return iterable
     */
    public function fetch() {
        return $this->entityManager->hydrateCollection($this->entityClass, $this->data);
    }

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function whereIn(string $field, array $value) {
        return $this;
    }
}