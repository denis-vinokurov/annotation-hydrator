<?php

namespace Vinds\AnnotationHydrator\Tests\ReferenceTest;

use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydrator\Annotations\Multiple;
use Vinds\AnnotationHydrator\Annotations\ReferenceField;
use Vinds\AnnotationHydrator\Reference\LazyValue;

/**
 * @Entity()
 * Class TestEntity1
 * @package Vinds\AnnotationHydrator\Tests\ReferenceTest
 */
class TestEntity1 {

    /**
     * @IntField(name="id")
     * @var int
     */
    public $id;

    /**
     * @ReferenceField(name="ref2", repository="repository2", referenceField="id")
     * @var TestEntity2
     */
    protected $ref2;

    /**
     * @Multiple()
     * @ReferenceField(name="ref3", repository="repository3", referenceField="id")
     * @var TestEntity3[]
     */
    protected $ref3;

    /**
     * @ReferenceField(name="oneToMany", repository="repository2", referenceField="manyToOne", oneToMany=true)
     * @var TestEntity2[]
     */
    protected $oneToMany;

    /**
     * @return TestEntity2
     */
    public function getRef2(): ?TestEntity2 {
        if ($this->ref2 instanceof LazyValue) {
            $this->ref2 = $this->ref2->get();
        }

        return $this->ref2;
    }

    /**
     * @return TestEntity3[]
     */
    public function getRef3(): ?array {
        if (is_array($this->ref3) && reset($this->ref3) instanceof LazyValue) {
            $this->ref3 = array_map(function (LazyValue $value) {
                return $value->get();
            }, $this->ref3);
        }
        return $this->ref3;
    }

    /**
     * @return TestEntity2[]
     */
    public function getOneToMany() {
        if ($this->oneToMany instanceof LazyValue) {
            $this->oneToMany = $this->oneToMany->get();
        }
        return $this->oneToMany;
    }
}