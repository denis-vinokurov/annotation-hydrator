<?php


namespace Vinds\AnnotationHydrator\Tests\ReferenceTest;


use Vinds\AnnotationHydrator\EntityManager;

class TestReferenceRepository implements \Vinds\AnnotationHydrator\Repository\RepositoryInterface {

    private $collection;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var string
     */
    private $entityClass;

    public function __construct($collection, EntityManager $entityManager, string $entityClass) {
        $this->collection = $collection;
        $this->entityManager = $entityManager;
        $this->entityClass = $entityClass;
    }

    /**
     * @return \Vinds\AnnotationHydrator\Repository\ListInterface
     */
    public function getList(): \Vinds\AnnotationHydrator\Repository\ListInterface {
        return new TestCollectionList($this->collection, $this->entityManager, $this->entityClass);
    }
}