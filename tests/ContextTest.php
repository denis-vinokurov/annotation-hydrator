<?php


namespace Vinds\AnnotationHydrator\Tests\ContextTest;


use Vinds\AnnotationHydrator\Mapping\Context;

class ContextTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \ReflectionClass
     */
    private $reflection;

    protected function setUp()
    {
        parent::setUp();

        $this->reflection = new \ReflectionClass(Context::class);
    }

    /**
     * Проверяем что уникальный индификатор будет уникален,
     * даже если хеш объекта был использован ранее для другого инстанса Context
     */
    public function testCreateUniqueIdentifier()
    {
        $context = new Context([], []);
        $hash = spl_object_hash($context);

        $propertyUniqueIdentifiers = $this->reflection->getProperty('uniqueIdentifiers');
        $propertyUniqueIdentifiers->setAccessible(true);
        $propertyUniqueIdentifiers->setValue($context, [
            spl_object_hash($context) => true
        ]);

        $method = $this->reflection->getMethod('createUniqueIdentifier');
        $method->setAccessible(true);

        $newHash = $method->invoke($context);

        $this->assertNotEquals($hash, $newHash);
    }

}