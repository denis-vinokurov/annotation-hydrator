<?php

namespace Vinds\AnnotationHydrator\Tests;

use Vinds\AnnotationHydrator\EntityManager;
use Zend\ServiceManager\ServiceManager;

class EntityManagerTest extends \PHPUnit\Framework\TestCase {

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     *
     */
    protected function setUp() {
        parent::setUp();

        $this->entityManager = new EntityManager(new ServiceManager(require __DIR__ . '/config.php'));
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreate() {
        /** @var EntityManager $em */
        $em = $this->entityManager;

        $date = (new \DateTime());

        /** @var EntityTest $entity */
        $entity = $em->hydrate(EntityTest::class, [
            'STRING'    => 'Строка',
            'INT'       => '100',
            'FLOAT'     => '0.100',
            'READ_ONLY' => 'readOnly',
            'MULTIPLE_STRING'  => [
                '1', '2', '3'
            ],
            'MULTIPLE_INT'  => [
                '1', '2', '3'
            ],
            'DATE_TIME' => $date->format($date::ISO8601),
        ]);

        $this->assertEquals($entity->commonProperty, 'testCommonProperty');
        $this->assertEquals($entity->string, 'Строка');
        $this->assertEquals($entity->int, (int)100);
        $this->assertEquals($entity->float, (float)0.10);
        $this->assertEquals($entity->readOnly, 'readOnly');
        $this->assertEquals($entity->dateTime->format(\DateTime::ISO8601), $date->format(\DateTime::ISO8601));

        foreach (['1', '2', '3'] as $key => $value) {
            $this->assertEquals($entity->multipleString[$key], $value);
        }
        foreach ([(int)1, (int)2, (int)3] as $key => $value) {
            $this->assertEquals($entity->multipleInt[$key], $value);
        }

        // Проверка создание пустого объекта
        $entity = $em->hydrate(EntityTest::class, []);


        $this->assertEquals($entity->commonProperty, 'testCommonProperty');
        $this->assertNull($entity->string);
        $this->assertNull($entity->int);
        $this->assertNull($entity->float);
        $this->assertNull($entity->readOnly);
        $this->assertNull($entity->dateTime);
        $this->assertNull($entity->multipleString);
        $this->assertNull($entity->multipleInt);
    }

    /**
     * Тест метода получение данных для сохранения
     * @throws \ReflectionException
     */
    public function testExtract() {
        $entity = new EntityTest();

        $entity->string = 'Строка';
        $entity->int = (int)100;
        $entity->float = (float)0.100;
        $entity->dateTime = new \DateTime();
        $entity->readOnly = 'readOnly';
        $entity->multipleString = [
            '1', '2', '3'
        ];
        $entity->multipleInt = [
            (int)1, (int)2, (int)3
        ];


        $data = $this->entityManager->extract($entity);

        $this->assertEquals($data['STRING'], 'Строка');
        $this->assertEquals($data['INT'], (int)100);
        $this->assertEquals($data['FLOAT'], (float)0.10);
        $this->assertEquals($data['DATE_TIME'], $entity->dateTime);
        $this->assertFalse(isset($data['READ_ONLY']));
        foreach ($entity->multipleString as $key => $value) {
            $this->assertEquals($data['MULTIPLE_STRING'][$key], $value);
        }
        foreach ($entity->multipleInt as $key => $value) {
            $this->assertEquals($data['MULTIPLE_INT'][$key], $value);
        }
    }

    /**
     * @throws \ReflectionException
     */
    public function testContext() {
        $data1 = [
            'IMAGE' => 1,
            'IMAGES' => [
                1, 2,
            ]
        ];

        $data2 = [
            'IMAGE' => 2,
            'IMAGES' => [
                1, 2, 3, 4
            ]
        ];

        /** @var ContextTest\Entity[] $collection */
        $collection = $this->entityManager->hydrateCollection(
            ContextTest\Entity::class,
            [
                $data1,
                $data2,
            ]
        );

        $entity1 = $collection[0];
        $entity2 = $collection[1];

        $this->assertEquals($entity1->image['id'], 1);
        $this->assertEquals($entity2->image['id'], 2);

        foreach ($data1['IMAGES'] as $key => $id) {
            $this->assertEquals($entity1->images[$key]['id'], $id);
        }

        foreach ($data2['IMAGES'] as $key => $id) {
            $this->assertEquals($entity2->images[$key]['id'], $id);
        }
    }

    /**
     * @throws \ReflectionException
     */
    public function testOptions() {
        $classMetadata = $this->entityManager->getClassMetadata(EntityTest::class);

        $options = [];
        foreach ($classMetadata->getFieldMappings() as $fieldMap) {
            if ($fieldMap->reflectionProperty->name === 'options') {
                $options = $fieldMap->options;
                break;
            }
        }

        $this->assertEquals($options['string'], 'value1');
        $this->assertEquals($options['array']['int'], 1);
        $this->assertEquals($options['array']['string'], 'test');
        $this->assertEquals($options['array']['bool'], true);
        $this->assertTrue($options['options2']);

        $this->assertEquals($classMetadata->getEntityMap()->options['classOption'], 'value');
    }

    /**
     * @throws \ReflectionException
     */
    public function testCreatePrefixAndPostfix() {
        /** @var EntityTest $entity */
        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'prefix_PREFIX' => 'PREFIX',
            'POSTFIX_postfix' => 'POSTFIX',
            'prefix_PREFIX_AND_POSTFIX_postfix' => 'PREFIX_AND_POSTFIX',
        ]);

        $this->assertEquals($entity->prefix, 'PREFIX');
        $this->assertEquals($entity->postfix, 'POSTFIX');
        $this->assertEquals($entity->prefixAndPostfix, 'PREFIX_AND_POSTFIX');
    }

    /**
     * @throws \ReflectionException
     */
    public function testExtractPrefixAndPostfix() {
        $entity = new EntityTest();

        $entity->prefix = 'PREFIX';
        $entity->postfix = 'POSTFIX';
        $entity->prefixAndPostfix = 'PREFIX_AND_POSTFIX';

        $data = $this->entityManager->extract($entity);

        $this->assertEquals($data['prefix_PREFIX'], 'PREFIX');
        $this->assertEquals($data['POSTFIX_postfix'], 'POSTFIX');
        $this->assertEquals($data['prefix_PREFIX_AND_POSTFIX_postfix'], 'PREFIX_AND_POSTFIX');
    }

    public function testSetPrimary() {
        $entity = new EntityTest();

        $this->entityManager->setPrimary($entity, '1');

        $this->assertEquals($entity->primary, 1);
    }

    /**
     *
     */
    public function testNotEmpty() {
        /** @var EntityTest $entity */
        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => 1,
        ]);
        $this->assertEquals(true, $entity->notEmpty);

        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => 's',
        ]);
        $this->assertEquals(true, $entity->notEmpty);

        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => true,
        ]);
        $this->assertEquals(true, $entity->notEmpty);



        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => '',
        ]);
        $this->assertEquals(false, $entity->notEmpty);

        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => false,
        ]);
        $this->assertEquals(false, $entity->notEmpty);

        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => null,
        ]);
        $this->assertEquals(false, $entity->notEmpty);

        $entity = $this->entityManager->hydrate(EntityTest::class, [
            'NOT_EMPTY' => 0.000,
        ]);
        $this->assertEquals(false, $entity->notEmpty);

    }
}
