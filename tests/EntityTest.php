<?php

namespace  Vinds\AnnotationHydrator\Tests;

use Vinds\AnnotationHydrator\Annotations\DateTimeField;
use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\FloatField;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydrator\Annotations\Multiple;
use Vinds\AnnotationHydrator\Annotations\Postfix;
use Vinds\AnnotationHydrator\Annotations\Prefix;
use Vinds\AnnotationHydrator\Annotations\StringField;
use Vinds\AnnotationHydrator\Annotations\Options;
use Vinds\AnnotationHydrator\Annotations;


/**
 * @Entity()
 * @Options({"classOption": "value"})
 * Class User
 * @package Vinds\AnnotationHydrator\Entity
 */
class EntityTest {

    /**
     * @Annotations\Primary()
     * @IntField(name="PRIMARY")
     * @var int
     */
    public $primary;

    /**
     * @StringField(name="STRING")
     * @var string
     */
    public $string;

    /**
     * @IntField(name="INT")
     * @var int
     */
    public $int;

    /**
     * @FloatField(name="FLOAT")
     * @var float
     */
    public $float;

    /**
     * @DateTimeField(name="DATE_TIME")
     * @var \DateTime
     */
    public $dateTime;

    /**
     * @StringField(name="READ_ONLY", readOnly=true)
     * @var string
     */
    public $readOnly;

    /**
     * @Multiple()
     * @StringField(name="MULTIPLE_STRING")
     * @var string[]
     */
    public $multipleString;

    /**
     * @Multiple()
     * @IntField(name="MULTIPLE_INT")
     * @var int[]
     */
    public $multipleInt;

    /**
     * @var mixed
     */
    public $commonProperty = 'testCommonProperty';

    /**
     * @Options({
     *     "string": "value1",
     *     "array": {"int": 1, "string": "test", "bool": true},
     *     @Annotations\Prefix(hydrate="CREATE_PREFIX", extract="EXTRACT_PREFIX"),
     *     @Annotations\Postfix(hydrate="CREATE_POSTFIX", extract="EXTRACT_POSTFIX"),
     * })
     * @Options({"options2": true})
     * @var array
     */
    public $options;

    /**
     * @Options({
     *     @Prefix(hydrate="prefix_", extract="prefix_"),
     * })
     * @Annotations\Field(name="PREFIX")
     * @var string
     */
    public $prefix;

    /**
     * @Options({
     *     @Postfix(hydrate="_postfix", extract="_postfix")
     * })
     * @Annotations\Field(name="POSTFIX")
     * @var string
     */
    public $postfix;
    /**
     * @Options({
     *     @Prefix(hydrate="prefix_", extract="prefix_"),
     *     @Postfix(hydrate="_postfix", extract="_postfix")
     * })
     * @Annotations\Field(name="PREFIX_AND_POSTFIX")
     * @var string
     */
    public $prefixAndPostfix;

    /**
     * @Annotations\NotEmptyField(name="NOT_EMPTY")
     * @var bool
     */
    public $notEmpty;

}