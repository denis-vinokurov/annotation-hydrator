<?php


namespace Vinds\AnnotationHydrator\Tests;


use PHPUnit\Framework\TestCase as TestCaseAlias;
use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\EntityManager;
use Vinds\AnnotationHydrator\Tests\ReferenceTest\TestEntity1;
use Vinds\AnnotationHydrator\Tests\ReferenceTest\TestEntity2;
use Vinds\AnnotationHydrator\Tests\ReferenceTest\TestEntity3;
use Vinds\AnnotationHydrator\Tests\ReferenceTest\TestReferenceRepository;
use Zend\ServiceManager\ServiceManager;

class ReferenceTest extends TestCaseAlias {

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     */
    protected function setUp() {
        parent::setUp();

        $this->container = new ServiceManager(array_merge_recursive(require __DIR__ . '/../tests/config.php', [
            'factories' => [

                EntityManager::class => function($container) {
                    return new EntityManager($container);
                },
                'repository1' => function (ContainerInterface $container) {
                    return new TestReferenceRepository([
                        [
                            'id' => 1,
                            'ref2' => 3,
                            'ref3' => [
                                10, 20, 30,
                            ],
                            'oneToMany' => 1
                        ],
                        [
                            'id' => 2,
                            'ref2' => 1,
                            'ref3' => [
                                50, 40,20
                            ],
                            'oneToMany' => 2,
                        ],
                        [
                            'id' => 5,
                        ],
                        [
                            'id' => 6,
                            'ref2' => null,
                            'ref3' => null,
                        ],
                    ], $container->get(EntityManager::class), TestEntity1::class);
                },
                'repository2' => function (ContainerInterface $container) {
                    return new TestReferenceRepository([
                        [
                            'id' => 1,
                            'manyToOne' => 1,
                        ],
                        [
                            'id' => 2,
                            'manyToOne' => 2,
                        ],
                        [
                            'id' => 3,
                            'manyToOne' => 1,
                        ],
                        [
                            'id' => 4,
                            'manyToOne' => 2,
                        ],
                        [
                            'id' => 5,
                            'manyToOne' => 1,
                        ],
                        [
                            'id' => 6,
                            'manyToOne' => 2,
                        ],
                    ], $container->get(EntityManager::class), TestEntity2::class);
                },
                'repository3' => function (ContainerInterface $container) {
                    return new TestReferenceRepository([
                        [
                            'id' => 10,
                        ],
                        [
                            'id' => 20,
                        ],
                        [
                            'id' => 30,
                        ],
                        [
                            'id' => 40,
                        ],
                        [
                            'id' => 50,
                        ],
                        [
                            'id' => 60,
                        ],
                    ], $container->get(EntityManager::class), TestEntity3::class);
                },
            ]
        ]));
        $this->entityManager = new EntityManager($this->container);
    }

    public function testHydrate() {
        /** @var TestEntity1[] $list1 */
        $list1 = $this->container->get('repository1')->getList()->fetch();
        /** @var TestEntity2[] $list2 */
        $list2 = $this->container->get('repository2')->getList()->fetch();
        /** @var TestEntity3[] $list3 */
        $list3 = $this->container->get('repository3')->getList()->fetch();


        $this->assertEquals($list1[0]->getRef2()->id, $list2[2]->id);
        $this->assertEquals($list1[0]->getRef3()[0]->id, $list3[0]->id);
        $this->assertEquals($list1[0]->getRef3()[1]->id, $list3[1]->id);
        $this->assertEquals($list1[0]->getRef3()[2]->id, $list3[2]->id);
        $this->assertEquals($list1[0]->getOneToMany()[0]->id, $list2[0]->id);
        $this->assertEquals($list1[0]->getOneToMany()[1]->id, $list2[2]->id);
        $this->assertEquals($list1[0]->getOneToMany()[2]->id, $list2[4]->id);

        $this->assertEquals($list1[1]->getRef2()->id, $list2[0]->id);
        $this->assertEquals($list1[1]->getRef3()[0]->id, $list3[4]->id);
        $this->assertEquals($list1[1]->getRef3()[1]->id, $list3[3]->id);
        $this->assertEquals($list1[1]->getRef3()[2]->id, $list3[1]->id);
        $this->assertEquals($list1[1]->getOneToMany()[0]->id, $list2[1]->id);
        $this->assertEquals($list1[1]->getOneToMany()[1]->id, $list2[3]->id);
        $this->assertEquals($list1[1]->getOneToMany()[2]->id, $list2[5]->id);

        $this->assertTrue($list1[0]->getOneToMany()[0]->manyToOne === $list1[0]);
        $this->assertTrue($list1[0]->getOneToMany()[1]->manyToOne === $list1[0]);
        $this->assertTrue($list1[0]->getOneToMany()[2]->manyToOne === $list1[0]);
        $this->assertTrue($list1[1]->getOneToMany()[0]->manyToOne === $list1[1]);
        $this->assertTrue($list1[1]->getOneToMany()[1]->manyToOne === $list1[1]);
        $this->assertTrue($list1[1]->getOneToMany()[2]->manyToOne === $list1[1]);

        $this->assertNull($list1[2]->getRef2());
        $this->assertNull($list1[2]->getRef3());

        $this->assertNull($list1[3]->getRef2());
        $this->assertNull($list1[3]->getRef3());
    }

    public function testExtract() {
        /** @var TestEntity1[] $list1 */
        $list1 = $this->container->get('repository1')->getList()->fetch();

        $element1 = $this->entityManager->extract($list1[0]);
        $element2 = $this->entityManager->extract($list1[1]);
        $element3 = $this->entityManager->extract($list1[2]);
        $element4 = $this->entityManager->extract($list1[3]);

        $this->assertEquals($element1['ref2'], 3);
        $this->assertEquals($element1['ref3'][0], 10);
        $this->assertEquals($element1['ref3'][1], 20);
        $this->assertEquals($element1['ref3'][2], 30);
        $this->assertEquals($element1['oneToMany'], 1);

        $this->assertEquals($element2['ref2'], 1);
        $this->assertEquals($element2['ref3'][0], 50);
        $this->assertEquals($element2['ref3'][1], 40);
        $this->assertEquals($element2['ref3'][2], 20);
        $this->assertEquals($element2['oneToMany'], 2);

        $this->assertNull($element3['ref2']);
        $this->assertNull($element3['ref3']);

        $this->assertNull($element4['ref2']);
        $this->assertNull($element4['ref3']);

        [
            [
                'id' => 1,
                'ref2' => 3,
                'ref3' => [
                    10, 20, 30,
                ],
                'oneToMany' => 1,
            ],
            [
                'id' => 2,
                'ref2' => 1,
                'ref3' => [
                    50, 40,20
                ],
                'oneToMany' => 2,
            ],
            [
                'id' => 5,
            ],
            [
                'id' => 6,
                'ref2' => null,
                'ref3' => null,
            ],
        ];
    }


}
