<?php


namespace Vinds\AnnotationHydrator\Tests\ContextTest;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyInterface;


class ImageStrategy implements StrategyInterface {

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context) {
        $cacheKey = spl_object_hash($context->parent()) . $fieldMap->hydrateName;
        $images = ImageRepository::getCache($cacheKey);

        if ($images === null) {
            if ($fieldMap->multiple) {
                $images = ImageRepository::find(array_reduce($context->parent()->getInput(), function($acc, $data) use ($fieldMap) {
                    return array_merge($acc, $data[$fieldMap->hydrateName]);
                }, []), $cacheKey);
            } else {
                $images = ImageRepository::find(array_map(function($data) use ($fieldMap) {
                    return $data[$fieldMap->hydrateName];
                }, $context->parent()->getInput()), $cacheKey);
            }

        }

        return $images[$value];
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }
}