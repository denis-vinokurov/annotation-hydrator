<?php


namespace Vinds\AnnotationHydrator\Tests\ContextTest;


class ImageRepository {

    protected static $cache = [];

    /**
     * @param $ids
     * @param $cacheKey
     * @return array|mixed
     */
    public static function find($ids, $cacheKey) {
        $result = [];
        foreach ($ids as $id) {
            $result[$id] = [
                'id' => $id,
                'name' => "name{$id}",
                'path' => "path_to_image_with_id_{$id}",
            ];
        }

        static::$cache[$cacheKey] = $result;
        return $result;
    }

    /**
     * @param $cacheKey
     * @return mixed|null
     */
    public static function getCache($cacheKey) {
        if (isset(static::$cache[$cacheKey])) {
            return static::$cache[$cacheKey];
        }
        return null;
    }
}