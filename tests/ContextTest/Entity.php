<?php


namespace Vinds\AnnotationHydrator\Tests\ContextTest;
use Vinds\AnnotationHydrator\Annotations\Multiple;


/**
 * @\Vinds\AnnotationHydrator\Annotations\Entity()
 * Class Entity
 * @package Vinds\AnnotationHydrator\Tests\ContextTest
 */
class Entity {

    /**
     * @AnnotationImage(name="IMAGE")
     * @var array
     */
    public $image;

    /**
     * @Multiple()
     * @AnnotationImage(name="IMAGES")
     * @var array
     */
    public $images;
}