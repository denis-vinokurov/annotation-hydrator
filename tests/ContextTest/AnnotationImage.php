<?php


namespace Vinds\AnnotationHydrator\Tests\ContextTest;
use Doctrine\Common\Annotations\Annotation\Target;
use Vinds\AnnotationHydrator\Annotations\Field;


/**
 * @Annotation
 * @Target({"PROPERTY"})
 * Class AnnotationImage
 * @package Vinds\AnnotationHydrator\Tests\ContextTest
 */
class AnnotationImage extends Field {

    public $strategy = ImageStrategy::class;
}