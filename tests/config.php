<?php

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;

return [
    'factories' => [
        \Doctrine\Common\Annotations\Reader::class => function() {
            return new CachedReader(
                new AnnotationReader(),
                new \Doctrine\Common\Cache\ArrayCache(),
                true
            );
        },

        \Doctrine\Common\Cache\Cache::class => function() {
            return new \Doctrine\Common\Cache\ArrayCache();
        }
    ]
];