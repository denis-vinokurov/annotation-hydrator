<?php


namespace Vinds\AnnotationHydrator\Tests\StrategyTest;


use Psr\Container\ContainerInterface;
use Vinds\AnnotationHydrator\Mapping\Context;
use Vinds\AnnotationHydrator\Mapping\FieldMap;
use Vinds\AnnotationHydrator\Strategy\StrategyWithContainerInterface;

class StrategyWithContainer implements StrategyWithContainerInterface {
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * StrategyWithContainerInterface constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Преобразует заданное значение при создание объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function hydrate($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }

    /**
     * Преобразует заданное значение при извлечение данных из объекта
     *
     * @param $value
     * @param FieldMap $fieldMap
     * @param null|Context $context
     * @return mixed
     */
    public function extract($value, FieldMap $fieldMap, ?Context $context) {
        return $value;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface {
        return $this->container;
    }
}