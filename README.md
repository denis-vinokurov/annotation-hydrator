## BOOTSTRAP
```php

$loader = require __DIR__ . '/../vendor/autoload.php';

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader([$loader, 'loadClass']);
```

## Класс сущности
```php
/**
 * @\Vinds\AnnotationHydrator\Annotations\Entity();
 *
 * Class User
 * @package Vinds\AnnotationHydrator\Entity
 */
class EntityTest {

    /**
     * @\Vinds\AnnotationHydrator\Annotations\StringField(name="STRING")
     * @var string
     */
    public $string;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\IntField(name="INT")
     * @var int
     */
    public $int;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\FloatField(name="FLOAT")
     * @var float
     */
    public $float;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\DateTimeField(name="DATE_TIME")
     * @var DateTime
     */
    public $dateTime;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\StringField(name="READ_ONLY", readOnly=true)
     * @var string
     */
    public $readOnly;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\Multiple()
     * @\Vinds\AnnotationHydrator\Annotations\StringField(name="MULTIPLE_STRING")
     * @var string[]
     */
    public $multipleString;

    /**
     * @\Vinds\AnnotationHydrator\Annotations\Multiple()
     * @\Vinds\AnnotationHydrator\Annotations\IntField(name="MULTIPLE_INT")
     * @var int[]
     */
    public $multipleInt;

    /**
     * @var mixed
     */
    public $commonProperty = 'testCommonProperty';
}
```

##Создание менеджера сущностей, на примере \Zend\ServiceManager
```php

$container = new \Zend\ServiceManager\ServiceManager([
     'factories' => [
         \Doctrine\Common\Annotations\Reader::class => function() {
             return new CachedReader(
                 new AnnotationReader(),
                 new \Doctrine\Common\Cache\ArrayCache(),
                 true
             );
         },
 
         \Doctrine\Common\Cache\Cache::class => function() {
             return new \Doctrine\Common\Cache\ArrayCache();
         }
     ]
 ]);

$entityManager = new \Vinds\AnnotationHydrator\EntityManager($container);
```

##Создание сущности
```php
$entity = $entityManager->create(EntityTest::class, [
    'STRING'    => 'Строка',
    'INT'       => '100',
    'FLOAT'     => '0.100',
    'READ_ONLY' => 'readOnly',
    'MULTIPLE_STRING'  => [
        '1', '2', '3'
    ],
    'MULTIPLE_INT'  => [
        '1', '2', '3'
    ],
    'DATE_TIME' => (new \DateTime())->format($date::ISO8601),
]);
```

##Получение исходных данных
```php
$data = $this->entityManager->extract($entity);
```

##Тип поля "Привязка к элементам"
* В параметре `repository` указывается ключ репозитория поторому его можно получить из контайнера.
* В параметре `referenceField` указывается поля, по которому происходит привязка
* Репозиторий  должен соответсвовать интерфейсу `\Vinds\AnnotationHydrator\Repository\RepositoryInterface`

```php

use Vinds\AnnotationHydrator\Annotations\Entity;
use Vinds\AnnotationHydrator\Annotations\IntField;
use Vinds\AnnotationHydrator\Annotations\Multiple;
use Vinds\AnnotationHydrator\Annotations\ReferenceField;
use Vinds\AnnotationHydrator\Reference\LazyValue;

/**
 * @Entity()
 * Class Entity
 */
class Entity {

    /**
     * @IntField(name="id")
     * @var int
     */
    public $id;

    /**
     * @ReferenceField(name="ref2", repository="repository2", referenceField="id")
     * @var TestEntity2
     */
    protected $ref2;

    /**
     * @Multiple()
     * @ReferenceField(name="ref3", repository="repository3", referenceField="id")
     * @var TestEntity3[]
     */
    protected $ref3;

    /**
     * @return TestEntity2
     */
    public function getRef2(): ?TestEntity2 {
        if ($this->ref2 instanceof LazyValue) {
            $this->ref2 = $this->ref2->get();
        }

        return $this->ref2;
    }

    /**
     * @return TestEntity3[]
     */
    public function getRef3(): ?array {
        if (is_array($this->ref3) && reset($this->ref3) instanceof LazyValue) {
            $this->ref3 = array_map(function (LazyValue $value) {
                return $value->get();
            }, $this->ref3);
        }
        return $this->ref3;
    }
}
```
