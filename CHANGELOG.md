## [0.9]() (20.12.2018)

### Features

* **\Vinds\AnnotationHydrator\Mapping\Context::getUniqueIdentifier** - уникальный идефикатор у контекста

## [0.8.1]() (11.08.2018)

### Bug fixes

* **\Vinds\AnnotationHydrator\Mapping\FieldMap** - ранее при попытки серилизовать объект падало с ошибкой
* Ключи кешей создаються с помощью sha1
* **\Vinds\AnnotationHydrator\EntityManager::fillFieldMapping** - исправлена ошибка, в результате которой при получении данных из кеша не записывалась статегия в поля

## [0.8]() (26.06.2018)

### BREAKING CHANGES

* **\Vinds\AnnotationHydrator\Repository\ListInterface::where** - метод удален
* **\Vinds\AnnotationHydrator\Repository\ListInterface::whereIn** - заменяет удаленный метод `ListInterface::where`
* **\Vinds\AnnotationHydrator\Mapping\Context::__construct** - теперь принимает 3 параметра: `$input` - исходные данные, `$output` - результирующие данные, `$parent` - родительский контекст
* **\Vinds\AnnotationHydrator\Mapping\Context::get** - заменен на метод `\Vinds\AnnotationHydrator\Mapping\Context::getInput`

### Features

* **\Vinds\AnnotationHydrator\Annotations\ReferenceField::$oneToMany** - связь типа один ко многим
* **\Vinds\AnnotationHydrator\Annotations\ReferenceField::$manyToOne** - связь типа многие к одному

## [0.7]() (04.06.2018)

### BREAKING CHANGES

* **\Vinds\AnnotationHydrator\EntityManager::__construct** - теперь для создание инстанса класса в конструктор необходимо передавать реализацию интерфейса \Psr\Container\ContainerInterface. В контейнере должны быть реализации интерфейсов: \Doctrine\Common\Annotations\Reader и \Doctrine\Common\Cache\Cache
* **\Vinds\AnnotationHydrator\Annotations\Field::$preSave** - Свойство удалено
* **\Vinds\AnnotationHydrator\Annotations\Field::$preSetValue** - Свойство удалено
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::$prepareToSave** - Свойство удалено
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::$prepareToSetValue** - Свойство удалено
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::$createName** - Свойство удалено
* **\Vinds\AnnotationHydrator\Annotations\Postfix::$create** - Свойство удалено
* **\Vinds\AnnotationHydrator\Annotations\Prefix::$create** - Свойство удалено
* **\Vinds\AnnotationHydrator\EntityManager::create** - Метод удален
* **\Vinds\AnnotationHydrator\EntityManager::createCollection** - Метод удален

### Bug fixes

* **\Vinds\AnnotationHydrator\EntityManager::extract** - при обработке множественных полей в метод `\Vinds\AnnotationHydrator\Strategy\StrategyInterface::extract` передавались все значение за раз, а не каждое по отдельности

### Features

* **Теперь стратегии получаются из контейнера, если их там нет то создаются по старинке**
* **\Vinds\AnnotationHydrator\Strategy\StrategyWithContainerInterface** - Интерфейс для стратегий, которым не обходим контейнер
* **\Vinds\AnnotationHydrator\Annotations\ReferenceField** - Поля привязки к элементу


## [0.6.4]() (25.05.2018)

### Bug fixes

* **\Vinds\AnnotationHydrator\Strategy\NotEmptyStrategy::extract** - возвращаемый тип bool|null

## [0.6.3]() (09.05.2018)

### Features

* **\Vinds\AnnotationHydrator\Annotations\NotEmptyField** - тип поля NotEmpty

## [0.6.1]() (09.05.2018)

### Features

* **\Vinds\AnnotationHydrator\Strategy** - указание возвращаемых типов

## [0.6]() (23.04.2018)

### Features

* **\Vinds\AnnotationHydrator\Strategy\StrategyInterface** - интерфейс для реализации стратегии записи и извлечение данных
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::strategy** - статегия записи и извлечения данных. заменит `\Vinds\AnnotationHydrator\Mapping\FieldMap::prepareToSave` и `\Vinds\AnnotationHydrator\Mapping\FieldMap::prepareToSetValue`
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::hydrateName** - название поля в оригинальном массиве

### Deprecated
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::createName** - помечен как усторевший будет удален в версии 0.7, использовать `\Vinds\AnnotationHydrator\Mapping\FieldMap::hydrateName`
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::prepareToSave** - помечен как усторевший будет удален в версии 0.7, использовать `\Vinds\AnnotationHydrator\Mapping\FieldMap::strategy`
* **\Vinds\AnnotationHydrator\Mapping\FieldMap::prepareToSetValue** - помечен как усторевший будет удален в версии 0.7, использовать `\Vinds\AnnotationHydrator\Mapping\FieldMap::strategy`
* **\Vinds\AnnotationHydrator\Annotations\Prefix::create** - помечен как усторевший будет удален в версии 0.7, использовать `\Vinds\AnnotationHydrator\Annotations\Prefix::hydrate`
* **\Vinds\AnnotationHydrator\Annotations\Postfix::create** - помечен как усторевший будет удален в версии 0.7, использовать `\Vinds\AnnotationHydrator\Annotations\Postfix::hydrate`


## [0.5]() (23.04.2018)

### Features

* **\Vinds\AnnotationHydrator\Mapping\EntityMap** - описание класса
* **\Vinds\AnnotationHydrator\Annotations\Options** - добавлена возможность указывать аннотацию у классов
